package com.example.test.restController;

import com.example.test.dto.penumpang.InsertPenumpangDTO;
import com.example.test.dto.penumpang.PenumpangDTO;
import com.example.test.dto.travel.InsertTravelDTO;
import com.example.test.dto.travel.TravelDTO;
import com.example.test.exception.NotFoundException;
import com.example.test.service.TravelService;
import com.example.test.util.ApiResponse;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/travel")
public class TravelRestController {

    @Autowired
    private TravelService travelService;

    @PostMapping("/")
    public ResponseEntity<List<TravelDTO>> getAllTravel() {

        List<TravelDTO> travelList = travelService.getAllTravel();

        return new ResponseEntity<>(travelList, HttpStatus.OK);

    }

    //soal no 3
    @PostMapping("/find/{search}")
    public ResponseEntity<Object> getTravel(@PathVariable String search) {

        List<TravelDTO> travelList  = travelService.getTravel(search);
        if (travelList != null) {
            return new ResponseEntity<>(travelList, HttpStatus.OK);
        } else {
            ApiResponse apiResponse = new ApiResponse("Travel Not Found", HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<>(apiResponse,HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<InsertTravelDTO> createTravel(@Valid @RequestBody InsertTravelDTO insertTravelDTO) {

        InsertTravelDTO savedTravelDTO = travelService.createTravel(insertTravelDTO);

        return new ResponseEntity<>(savedTravelDTO, HttpStatus.CREATED);

    }

    @PostMapping("/update/{id}")
    public ResponseEntity<Object> updateTravel(@PathVariable Integer id, @Valid @RequestBody InsertTravelDTO insertTravelDTO) {

            InsertTravelDTO updatedTravelDTO = travelService.updateTravel(id, insertTravelDTO);
            return new ResponseEntity<>(updatedTravelDTO, HttpStatus.OK);

    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteTravel(@PathVariable Integer id) {
        try {
            travelService.deleteTravelById(id);
            ApiResponse apiResponse = new ApiResponse("Travel Successfully Deleted", HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<>(apiResponse,HttpStatus.NOT_FOUND);
        } catch (RuntimeException e) {
            throw new NotFoundException(e.getMessage());
        }
    }


}
