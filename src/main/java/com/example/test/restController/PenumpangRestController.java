package com.example.test.restController;

import com.example.test.dto.penumpang.InsertPenumpangDTO;
import com.example.test.dto.penumpang.PenumpangDTO;
import com.example.test.service.PenumpangService;
import com.example.test.util.ApiResponse;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/penumpang")
public class PenumpangRestController {

    @Autowired
    private PenumpangService penumpangService;

    @PostMapping("/")
    public ResponseEntity<List<PenumpangDTO>> getAllPenumpang() {

        List<PenumpangDTO> penumpangDTOList = penumpangService.getAllPenumpang();

            return new ResponseEntity<>(penumpangDTOList, HttpStatus.OK);

    }

    @PostMapping("/find/{id}")
    public ResponseEntity<Object> getPenumpangById(@PathVariable Integer id) {

        PenumpangDTO penumpangDTO  = penumpangService.getPenumpangById(id);
        if (penumpangDTO != null) {
            return new ResponseEntity<>(penumpangDTO, HttpStatus.OK);
        } else {
            ApiResponse apiResponse = new ApiResponse("Passenger Not Found", HttpStatus.NOT_FOUND.value());
            return new ResponseEntity<>(apiResponse,HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<InsertPenumpangDTO> createPenumpang(@Valid @RequestBody InsertPenumpangDTO insertPenumpangDTO) {

        InsertPenumpangDTO savedPenumpangDTO = penumpangService.createPenumpang(insertPenumpangDTO);

        return new ResponseEntity<>(savedPenumpangDTO, HttpStatus.CREATED);

    }

    @PostMapping("/update/{id}")
    public ResponseEntity<Object> updatePenumpang(@PathVariable Integer id, @Valid @RequestBody InsertPenumpangDTO insertPenumpangDTO) {

            InsertPenumpangDTO updatedPenumpangDTO = penumpangService.updatePenumpang(id, insertPenumpangDTO);
            return new ResponseEntity<>(updatedPenumpangDTO, HttpStatus.OK);

    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deletePenumpang(@PathVariable Integer id) {
            penumpangService.deletePenumpangById(id);
            ApiResponse apiResponse = new ApiResponse("Passenger Successfully deleted", HttpStatus.NO_CONTENT.value());
            return new ResponseEntity<>(apiResponse,HttpStatus.NO_CONTENT);
    }


}
