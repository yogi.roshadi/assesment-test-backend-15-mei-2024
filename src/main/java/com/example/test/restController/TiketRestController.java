package com.example.test.restController;

import com.example.test.dto.tiket.InsertTiketDTO;
import com.example.test.dto.tiket.TiketDTO;
import com.example.test.dto.travel.InsertTravelDTO;
import com.example.test.dto.travel.TravelDTO;
import com.example.test.exception.NotFoundException;
import com.example.test.service.TiketService;
import com.example.test.util.ApiResponse;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tiket")
public class TiketRestController {

    @Autowired
    private TiketService tiketService;


    @PostMapping("/")
    public ResponseEntity<List<TiketDTO>> getAllTicket() {

        List<TiketDTO> ticketList = tiketService.getAllTicket();

        return new ResponseEntity<>(ticketList, HttpStatus.OK);

    }

    @PostMapping("/find/{search}")
    public ResponseEntity<Object> findTicketById(@PathVariable Integer search) {

        TiketDTO tiket  = tiketService.findTicketById(search);
        if (tiket != null) {
            return new ResponseEntity<>(tiket, HttpStatus.OK);
        } else {
            throw new NotFoundException("Ticket with id " + search + " not found!");
        }
    }


    @PostMapping("/create")
    public ResponseEntity<InsertTiketDTO> createTicket(@Valid @RequestBody InsertTiketDTO insertTiketDTO) {

        InsertTiketDTO savedTicketDTO = tiketService.createTicket(insertTiketDTO);

        return new ResponseEntity<>(savedTicketDTO, HttpStatus.CREATED);

    }

    @PostMapping("/update/{id}")
    public ResponseEntity<Object> updateTicket(@PathVariable Integer id,@Valid @RequestBody InsertTiketDTO insertTiketDTO) {

            InsertTiketDTO updatedTiketDTO = tiketService.updateTiket(id, insertTiketDTO);
            return new ResponseEntity<>(updatedTiketDTO, HttpStatus.OK);


    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteTicket(@PathVariable Integer id) {
        try {
            tiketService.deleteTicketById(id);
            ApiResponse apiResponse = new ApiResponse("Ticket Successfully Deleted", HttpStatus.OK.value());
            return new ResponseEntity<>(apiResponse,HttpStatus.OK);
        } catch (RuntimeException e) {
            throw new NotFoundException(e.getMessage());
        }
    }


}
