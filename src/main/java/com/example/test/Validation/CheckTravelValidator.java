package com.example.test.Validation;

import com.example.test.service.PenumpangService;
import com.example.test.service.TravelService;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

public class CheckTravelValidator implements ConstraintValidator<CheckTravel, Integer> {
    @Autowired
    private TravelService travelService;

    @Override
    public boolean isValid(Integer id_travel, ConstraintValidatorContext constraintValidatorContext) {

        return !travelService.checkExistingTravel(id_travel);
    }
}
