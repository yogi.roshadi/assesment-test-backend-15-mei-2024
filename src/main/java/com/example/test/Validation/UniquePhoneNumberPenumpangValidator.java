package com.example.test.Validation;

import com.example.test.service.PenumpangService;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;

public class UniquePhoneNumberPenumpangValidator implements ConstraintValidator<UniquePhoneNumberPenumpang, Object> {

    private String phoneNumber;

    @Autowired
    private PenumpangService penumpangService;


    @Override
    public void initialize(UniquePhoneNumberPenumpang constraintAnnotation) {
        this.phoneNumber =constraintAnnotation.phoneNumber();
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext constraintValidatorContext) {
        String phoneNumberValue = new BeanWrapperImpl(object).getPropertyValue(phoneNumber).toString();

        return !penumpangService.checkExistingPhoneNumber(phoneNumberValue);
    }
}
