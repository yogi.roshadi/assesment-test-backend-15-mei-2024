package com.example.test.Validation;

import com.example.test.service.PenumpangService;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

public class CheckPenumpangValidator implements ConstraintValidator<CheckPenumpang, Integer> {

    @Autowired
    private PenumpangService penumpangService;

    @Override
    public boolean isValid(Integer id_penumpang, ConstraintValidatorContext constraintValidatorContext) {
        return !penumpangService.checkExistingPenumpang(id_penumpang);
    }
}
