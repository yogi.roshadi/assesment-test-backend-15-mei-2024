package com.example.test.dto.tiket;

import com.example.test.Validation.CheckPenumpang;
import com.example.test.Validation.CheckTravel;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

public class InsertTiketDTO {

    @NotNull
    @CheckPenumpang(message = "Penumpang is not exist!")
    private Integer id_penumpang;
    @NotNull
    @CheckTravel(message = "Travel is not exist!")
    private Integer id_travel;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime jadwal;

    public InsertTiketDTO() {
    }

    public InsertTiketDTO(Integer id_penumpang, Integer id_travel, LocalDateTime jadwal) {
        this.id_penumpang = id_penumpang;
        this.id_travel = id_travel;
        this.jadwal = jadwal;
    }

    public Integer getId_penumpang() {
        return id_penumpang;
    }

    public void setId_penumpang(Integer id_penumpang) {
        this.id_penumpang = id_penumpang;
    }

    public Integer getId_travel() {
        return id_travel;
    }

    public void setId_travel(Integer id_travel) {
        this.id_travel = id_travel;
    }

    public LocalDateTime getJadwal() {
        return jadwal;
    }

    public void setJadwal(LocalDateTime jadwal) {
        this.jadwal = jadwal;
    }
}
