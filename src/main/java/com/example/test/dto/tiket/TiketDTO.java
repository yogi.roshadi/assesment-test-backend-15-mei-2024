package com.example.test.dto.tiket;


import com.example.test.entity.Tiket;

import java.time.LocalDateTime;

public class TiketDTO {

    private Integer id;
    private Integer id_penumpang;
    private Integer id_travel;
    private LocalDateTime jadwal;

    public TiketDTO() {
    }

    public TiketDTO(Integer id, Integer id_penumpang, Integer id_travel, LocalDateTime jadwal) {
        this.id = id;
        this.id_penumpang = id_penumpang;
        this.id_travel = id_travel;
        this.jadwal = jadwal;
    }

    public TiketDTO(Tiket tiket) {
        this.id = tiket.getId();
        this.id_penumpang = tiket.getId_penumpang();
        this.id_travel = tiket.getId_travel();
        this.jadwal = tiket.getJadwal();
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_penumpang() {
        return id_penumpang;
    }

    public void setId_penumpang(Integer id_penumpang) {
        this.id_penumpang = id_penumpang;
    }

    public Integer getId_travel() {
        return id_travel;
    }

    public void setId_travel(Integer id_travel) {
        this.id_travel = id_travel;
    }

    public LocalDateTime getJadwal() {
        return jadwal;
    }

    public void setJadwal(LocalDateTime jadwal) {
        this.jadwal = jadwal;
    }
}
