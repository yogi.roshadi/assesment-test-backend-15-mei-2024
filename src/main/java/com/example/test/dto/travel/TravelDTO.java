package com.example.test.dto.travel;

import com.example.test.entity.Travel;

public class TravelDTO {
    private Integer id;
    private String nameTravel;
    private String phoneNumber;
    private String address;
    private String policeNo;
    private String busType;

    public TravelDTO() {
    }

    public TravelDTO(Integer id,String nameTravel, String phoneNumber, String address, String policeNo, String busType) {
        this.id = id;
        this.nameTravel = nameTravel;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.policeNo = policeNo;
        this.busType = busType;
    }

    public TravelDTO(Travel travel) {
        this.id = travel.getId();
        this.nameTravel = travel.getNameTravel();
        this.phoneNumber = travel.getPhoneNumber();
        this.address = travel.getAddress();
        this.policeNo = travel.getPoliceNo();
        this.busType = travel.getBusType();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameTravel() {
        return nameTravel;
    }

    public void setNameTravel(String nameTravel) {
        this.nameTravel = nameTravel;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPoliceNo() {
        return policeNo;
    }

    public void setPoliceNo(String policeNo) {
        this.policeNo = policeNo;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }
}
