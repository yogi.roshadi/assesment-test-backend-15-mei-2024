package com.example.test.dto.travel;

import jakarta.validation.constraints.NotBlank;

public class InsertTravelDTO {

    @NotBlank
    private String nameTravel;
    private String phoneNumber;
    private String address;
    @NotBlank
    private String policeNo;
    private String busType;

    public InsertTravelDTO() {
    }

    public InsertTravelDTO(String nameTravel, String phoneNumber, String address, String policeNo, String busType) {
        this.nameTravel = nameTravel;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.policeNo = policeNo;
        this.busType = busType;
    }

    public String getNameTravel() {
        return nameTravel;
    }

    public void setNameTravel(String nameTravel) {
        this.nameTravel = nameTravel;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPoliceNo() {
        return policeNo;
    }

    public void setPoliceNo(String policeNo) {
        this.policeNo = policeNo;
    }

    public String getBusType() {
        return busType;
    }

    public void setBusType(String busType) {
        this.busType = busType;
    }
}
