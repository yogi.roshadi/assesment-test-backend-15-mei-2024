package com.example.test.dto.penumpang;

import com.example.test.Validation.UniquePhoneNumberPenumpang;
import jakarta.validation.constraints.NotBlank;

@UniquePhoneNumberPenumpang(message = "Phone Number already exist!", phoneNumber = "phoneNumber")
public class InsertPenumpangDTO {

    @NotBlank
    private String nama;
    @NotBlank
    private String phoneNumber;

    public InsertPenumpangDTO() {
    }

    public InsertPenumpangDTO(String nama, String phoneNumber) {
        this.nama = nama;
        this.phoneNumber = phoneNumber;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
