package com.example.test.dto.penumpang;

import com.example.test.entity.Penumpang;

public class PenumpangDTO {

    private Integer id_penumpang;
    private String nama;
    private String phoneNumber;

    public PenumpangDTO() {
    }

    public PenumpangDTO(Integer id_penumpang, String nama, String phoneNumber) {
        this.id_penumpang = id_penumpang;
        this.nama = nama;
        this.phoneNumber = phoneNumber;
    }

    public PenumpangDTO(Penumpang penumpang) {
        this.id_penumpang = penumpang.getId_penumpang();
        this.nama = penumpang.getNama();
        this.phoneNumber = penumpang.getPhoneNumber();
    }


    public Integer getId_penumpang() {
        return id_penumpang;
    }

    public void setId_penumpang(Integer id_penumpang) {
        this.id_penumpang = id_penumpang;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
