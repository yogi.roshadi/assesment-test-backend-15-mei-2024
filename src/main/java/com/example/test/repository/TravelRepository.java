package com.example.test.repository;

import com.example.test.dto.penumpang.PenumpangDTO;
import com.example.test.dto.travel.TravelDTO;
import com.example.test.entity.Travel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TravelRepository extends JpaRepository<Travel, Integer> {

    @Query("""
            SELECT new com.example.test.dto.travel.TravelDTO(a)
            FROM Travel a
            """)
    List<TravelDTO> findAllTravelDTO();

    @Query("""
            SELECT new com.example.test.dto.travel.TravelDTO(a)
            FROM Travel a
            WHERE LOWER(a.nameTravel) LIKE LOWER(CONCAT('%', :search, '%')) or
            LOWER(a.address) LIKE LOWER(CONCAT('%', :search, '%')) or
            LOWER(a.policeNo) LIKE LOWER(CONCAT('%', :search, '%'))
            """)
    List<TravelDTO> findListTravel(@Param("search") String search);


    @Query("""
            SELECT COUNT (*)
            FROM Travel a
            WHERE a.id = :idTravel
            """)
    Long countExistingTravel(@Param("idTravel") Integer id_travel);
}
