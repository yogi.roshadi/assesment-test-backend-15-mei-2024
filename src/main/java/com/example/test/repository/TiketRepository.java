package com.example.test.repository;

import com.example.test.dto.tiket.TiketDTO;
import com.example.test.entity.Tiket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TiketRepository extends JpaRepository<Tiket, Integer> {

    @Query("""
            SELECT new com.example.test.dto.tiket.TiketDTO(a)
            FROM Tiket a
            """)
    List<TiketDTO> findAllTicketDTO();

    @Query("""
            SELECT new com.example.test.dto.tiket.TiketDTO(a)
            FROM Tiket a
            WHERE a.id = :search
            """)
    TiketDTO findTicketById(@Param("search") Integer search);
}
