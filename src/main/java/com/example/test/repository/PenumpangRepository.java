package com.example.test.repository;

import com.example.test.dto.penumpang.PenumpangDTO;
import com.example.test.entity.Penumpang;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PenumpangRepository extends JpaRepository<Penumpang, Integer> {


    @Query("""
            SELECT new com.example.test.dto.penumpang.PenumpangDTO(a.id_penumpang, a.nama, a.phoneNumber)
            FROM Penumpang a
            """)
    List<PenumpangDTO> findAllPenumpangDTO();

    @Query("""
            SELECT new com.example.test.dto.penumpang.PenumpangDTO(a)
            FROM Penumpang a
            WHERE a.id_penumpang = :id
            """)
    PenumpangDTO findPenumpangById(@Param("id") Integer id);


    @Query("""
            SELECT COUNT (*)
            FROM Penumpang a
            WHERE a.id_penumpang = :idPenumpang
            """)
    Long countExistingPenumpang(@Param("idPenumpang") Integer idPenumpang);

    @Query("""
            SELECT COUNT (*)
            FROM Penumpang a
            WHERE a.phoneNumber = :phoneNumberValue
            """)
    Long countExistingPhoneNumber(@Param("phoneNumberValue") String phoneNumberValue);
}
