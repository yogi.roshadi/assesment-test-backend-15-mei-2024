package com.example.test.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "Penumpang")
public class Penumpang {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_penumpang")
    private Integer id_penumpang;

    @Column(name = "nama")
    private String nama;

    @Column(name = "no_telp")
    private String phoneNumber;

    @JsonIgnore
    @OneToMany(mappedBy = "penumpang")
    private List<Tiket> tiketList;

    public Penumpang() {
    }

    public Penumpang(String nama, String phoneNumber) {
        this.nama = nama;
        this.phoneNumber = phoneNumber;
    }

    public Integer getId_penumpang() {
        return id_penumpang;
    }

    public void setId_penumpang(Integer id_penumpang) {
        this.id_penumpang = id_penumpang;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
