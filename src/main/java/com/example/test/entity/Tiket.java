package com.example.test.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.time.LocalDateTime;

@Entity
@Table(name = "Tiket")
public class Tiket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "id_penumpang")
    private Integer id_penumpang;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_penumpang", insertable = false, updatable = false)
    private Penumpang penumpang;

    @Column(name = "id_travel")
    private Integer id_travel;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id_travel", insertable = false, updatable = false)
    private Travel travel;

    @Column(name = "Jadwal")
    private LocalDateTime jadwal;

    public Tiket() {
    }

    public Tiket(Integer id_penumpang, Penumpang penumpang, Integer id_travel, Travel travel, LocalDateTime jadwal) {
        this.id_penumpang = id_penumpang;
        this.penumpang = penumpang;
        this.id_travel = id_travel;
        this.travel = travel;
        this.jadwal = jadwal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_penumpang() {
        return id_penumpang;
    }

    public void setId_penumpang(Integer id_penumpang) {
        this.id_penumpang = id_penumpang;
    }

    public Integer getId_travel() {
        return id_travel;
    }

    public void setId_travel(Integer id_travel) {
        this.id_travel = id_travel;
    }

    public LocalDateTime getJadwal() {
        return jadwal;
    }

    public void setJadwal(LocalDateTime jadwal) {
        this.jadwal = jadwal;
    }
}
