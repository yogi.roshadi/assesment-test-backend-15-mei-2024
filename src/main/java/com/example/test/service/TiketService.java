package com.example.test.service;

import com.example.test.dto.tiket.InsertTiketDTO;
import com.example.test.dto.tiket.TiketDTO;

import java.util.List;

public interface TiketService {
    List<TiketDTO> getAllTicket();

    InsertTiketDTO createTicket(InsertTiketDTO insertTiketDTO);

    InsertTiketDTO updateTiket(Integer id, InsertTiketDTO insertTiketDTO);

    void deleteTicketById(Integer id);

    TiketDTO findTicketById(Integer search);
}
