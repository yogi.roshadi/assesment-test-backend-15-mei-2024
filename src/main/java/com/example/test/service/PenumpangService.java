package com.example.test.service;

import com.example.test.dto.penumpang.InsertPenumpangDTO;
import com.example.test.dto.penumpang.PenumpangDTO;

import java.util.List;

public interface PenumpangService {

    List<PenumpangDTO> getAllPenumpang();

    InsertPenumpangDTO createPenumpang(InsertPenumpangDTO insertPenumpangDTO);

    InsertPenumpangDTO updatePenumpang(Integer id, InsertPenumpangDTO insertPenumpangDTO);

    void deletePenumpangById(Integer id);

    PenumpangDTO getPenumpangById(Integer id);

    boolean checkExistingPenumpang(Integer idPenumpang);

    boolean checkExistingPhoneNumber(String phoneNumberValue);
}
