package com.example.test.service;

import com.example.test.dto.travel.InsertTravelDTO;
import com.example.test.dto.travel.TravelDTO;

import java.util.List;

public interface TravelService {
    List<TravelDTO> getAllTravel();

    InsertTravelDTO createTravel(InsertTravelDTO insertTravelDTO);

    InsertTravelDTO updateTravel(Integer id, InsertTravelDTO insertTravelDTO);

    List<TravelDTO> getTravel(String search);

    void deleteTravelById(Integer id);

    boolean checkExistingTravel(Integer id_travel);
}
