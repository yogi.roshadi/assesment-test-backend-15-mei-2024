package com.example.test.service.impl;

import com.example.test.dto.tiket.InsertTiketDTO;
import com.example.test.dto.tiket.TiketDTO;
import com.example.test.dto.travel.InsertTravelDTO;
import com.example.test.entity.Penumpang;
import com.example.test.entity.Tiket;
import com.example.test.entity.Travel;
import com.example.test.exception.NotFoundException;
import com.example.test.repository.PenumpangRepository;
import com.example.test.repository.TiketRepository;
import com.example.test.repository.TravelRepository;
import com.example.test.service.TiketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TiketServiceImpl implements TiketService {

    @Autowired
    private TiketRepository tiketRepository;

    @Autowired
    private PenumpangRepository penumpangRepository;

    @Autowired
    private TravelRepository travelRepository;


    @Override
    public List<TiketDTO> getAllTicket() {

        return tiketRepository.findAllTicketDTO();
    }

    @Override
    public TiketDTO findTicketById(Integer search) {

        return tiketRepository.findTicketById(search);
    }


    @Override
    public InsertTiketDTO createTicket(InsertTiketDTO insertTiketDTO) {
        Penumpang penumpang = penumpangRepository.findById(insertTiketDTO.getId_penumpang()).orElseThrow(() -> new NotFoundException("Passengger not found"));
        Travel travel = travelRepository.findById(insertTiketDTO.getId_travel()).orElseThrow(() -> new NotFoundException("Travel not found"));


        Tiket tiket = new Tiket(
                insertTiketDTO.getId_penumpang(),
                penumpang,
                insertTiketDTO.getId_travel(),
                travel,
                insertTiketDTO.getJadwal()
        );

        Tiket savedTiket = tiketRepository.save(tiket);

        InsertTiketDTO responseDTO = new InsertTiketDTO();

        responseDTO.setId_penumpang(savedTiket.getId_penumpang());
        responseDTO.setId_travel(savedTiket.getId_travel());
        responseDTO.setJadwal(savedTiket.getJadwal());
        return responseDTO;
    }

    @Override
    public InsertTiketDTO updateTiket(Integer id, InsertTiketDTO insertTiketDTO) {
        Optional<Tiket> optionalTiket = tiketRepository.findById(id);

        if (optionalTiket.isPresent()) {
            Tiket tiket = optionalTiket.get();

            if(insertTiketDTO.getId_penumpang() != null) {
                tiket.setId_penumpang(insertTiketDTO.getId_penumpang());
            }
            if(insertTiketDTO.getId_travel() != null) {
                tiket.setId_travel(insertTiketDTO.getId_travel());
            }
            if(insertTiketDTO.getJadwal() != null) {
                tiket.setJadwal(insertTiketDTO.getJadwal());
            }
            Tiket updatedTiket = tiketRepository.save(tiket);

            InsertTiketDTO responseDTO = new InsertTiketDTO();

            responseDTO.setId_penumpang(updatedTiket.getId_penumpang());
            responseDTO.setId_travel(updatedTiket.getId_travel());
            responseDTO.setJadwal(updatedTiket.getJadwal());
            return responseDTO;
        } else {
            throw new NotFoundException("Ticket with id " + id + " not found");
        }
    }

    @Override
    public void deleteTicketById(Integer id) {
        if (tiketRepository.existsById(id)) {
            tiketRepository.deleteById(id);
        } else {
            throw new NotFoundException("Ticket with id " + id + " not found");
        }
    }


}
