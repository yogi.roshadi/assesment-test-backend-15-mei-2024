package com.example.test.service.impl;

import com.example.test.dto.penumpang.InsertPenumpangDTO;
import com.example.test.dto.penumpang.PenumpangDTO;
import com.example.test.entity.Penumpang;
import com.example.test.exception.NotFoundException;
import com.example.test.repository.PenumpangRepository;
import com.example.test.service.PenumpangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PenumpangServiceImpl implements PenumpangService {

    @Autowired
    private PenumpangRepository penumpangRepository;


    public List<PenumpangDTO> getAllPenumpang() {

        return penumpangRepository.findAllPenumpangDTO();

    }


    @Override
    public InsertPenumpangDTO createPenumpang(InsertPenumpangDTO insertPenumpangDTO) {

        Penumpang penumpang = new Penumpang(
                insertPenumpangDTO.getNama(),
                insertPenumpangDTO.getPhoneNumber()
        );

        Penumpang savedPenumpang = penumpangRepository.save(penumpang);

        InsertPenumpangDTO responseDTO = new InsertPenumpangDTO();

        responseDTO.setNama(savedPenumpang.getNama());
        responseDTO.setPhoneNumber(savedPenumpang.getPhoneNumber());


        return responseDTO;
    }

    @Override
    public InsertPenumpangDTO updatePenumpang(Integer id, InsertPenumpangDTO insertPenumpangDTO) {
        Optional<Penumpang> optionalPenumpang = penumpangRepository.findById(id);

        if (optionalPenumpang.isPresent()) {
            Penumpang penumpang = optionalPenumpang.get();

            if(insertPenumpangDTO.getNama() != null) {
                penumpang.setNama(insertPenumpangDTO.getNama());
            }
            if(insertPenumpangDTO.getPhoneNumber() != null) {
                penumpang.setPhoneNumber(insertPenumpangDTO.getPhoneNumber());
            }

            Penumpang updatedPenumpang = penumpangRepository.save(penumpang);

            InsertPenumpangDTO responseDTO = new InsertPenumpangDTO();

            responseDTO.setNama(updatedPenumpang.getNama());
            responseDTO.setPhoneNumber(updatedPenumpang.getPhoneNumber());
            return responseDTO;
        } else {
            throw new NotFoundException("Penumpang with id " + id + " not found");
        }
    }

    @Override
    public void deletePenumpangById(Integer id) {
            if (penumpangRepository.existsById(id)) {
                penumpangRepository.deleteById(id);
            } else {
                throw new NotFoundException("Penumpang with id " + id + " not found");
            }
    }

    @Override
    public PenumpangDTO getPenumpangById(Integer id) {
        return penumpangRepository.findPenumpangById(id);
    }

    @Override
    public boolean checkExistingPenumpang(Integer idPenumpang) {
        Long totalPenumpang = penumpangRepository.countExistingPenumpang(idPenumpang);
        return totalPenumpang == 0;
    }

    @Override
    public boolean checkExistingPhoneNumber(String phoneNumberValue) {
        Long totalPhoneNumber = penumpangRepository.countExistingPhoneNumber(phoneNumberValue);

        return totalPhoneNumber > 0;
    }


}
