package com.example.test.service.impl;

import com.example.test.dto.penumpang.InsertPenumpangDTO;
import com.example.test.dto.travel.InsertTravelDTO;
import com.example.test.dto.travel.TravelDTO;
import com.example.test.entity.Penumpang;
import com.example.test.entity.Travel;
import com.example.test.exception.NotFoundException;
import com.example.test.repository.TravelRepository;
import com.example.test.service.TravelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TravelServiceImpl implements TravelService {

    @Autowired
    private TravelRepository travelRepository;

    @Override
    public List<TravelDTO> getAllTravel() {


        return travelRepository.findAllTravelDTO();
    }

    @Override
    public List<TravelDTO> getTravel(String search) {
        return travelRepository.findListTravel(search);
    }

    @Override
    public InsertTravelDTO createTravel(InsertTravelDTO insertTravelDTO) {
        Travel travel = new Travel(
                insertTravelDTO.getNameTravel(),
                insertTravelDTO.getPhoneNumber(),
                insertTravelDTO.getAddress(),
                insertTravelDTO.getPoliceNo(),
                insertTravelDTO.getBusType()
        );

        Travel savedTravel = travelRepository.save(travel);

        InsertTravelDTO responseDTO = new InsertTravelDTO();

        responseDTO.setNameTravel(savedTravel.getNameTravel());
        responseDTO.setPhoneNumber(savedTravel.getPhoneNumber());
        responseDTO.setAddress(savedTravel.getAddress());
        responseDTO.setPoliceNo(savedTravel.getPoliceNo());
        responseDTO.setBusType(savedTravel.getBusType());
        return responseDTO;
    }

    @Override
    public InsertTravelDTO updateTravel(Integer id, InsertTravelDTO insertTravelDTO) {
        Optional<Travel> optionalTravel = travelRepository.findById(id);

        if (optionalTravel.isPresent()) {
            Travel travel = optionalTravel.get();

            if(insertTravelDTO.getNameTravel() != null) {
                travel.setNameTravel(insertTravelDTO.getNameTravel());
            }
            if(insertTravelDTO.getPhoneNumber() != null) {
                travel.setPhoneNumber(insertTravelDTO.getPhoneNumber());
            }
            if(insertTravelDTO.getAddress() != null) {
                travel.setAddress(insertTravelDTO.getAddress());
            }
            if(insertTravelDTO.getPoliceNo() != null) {
                travel.setPoliceNo(insertTravelDTO.getPoliceNo());
            }
            if(insertTravelDTO.getBusType() != null) {
                travel.setBusType(insertTravelDTO.getBusType());
            }

            Travel updatedTravel = travelRepository.save(travel);

            InsertTravelDTO responseDTO = new InsertTravelDTO();

            responseDTO.setNameTravel(updatedTravel.getNameTravel());
            responseDTO.setPhoneNumber(updatedTravel.getPhoneNumber());
            responseDTO.setAddress(updatedTravel.getAddress());
            responseDTO.setPoliceNo(updatedTravel.getPoliceNo());
            responseDTO.setBusType(updatedTravel.getBusType());
            return responseDTO;
        } else {
            throw new NotFoundException("Travel with id " + id + " not found");
        }
    }



    @Override
    public void deleteTravelById(Integer id) {
        if (travelRepository.existsById(id)) {
            travelRepository.deleteById(id);
        } else {
            throw new NotFoundException("Travel with id " + id + " not found");
        }
    }



    @Override
    public boolean checkExistingTravel(Integer id_travel) {
        Long totalPenumpang = travelRepository.countExistingTravel(id_travel);
        return totalPenumpang == 0;
    }


}
